#!/bin/bash

requirements=("langchain" "fastapi" "pypdf" "sentence-transformers" "chromadb==0.3.29")

download_dir="python_packages_for_llm_rag"

function install_package() {
    for package in "${requirements[@]}"; do
        package_dir="$download_dir/$package"
        echo $package
        pip install "$package" --no-index --find-links "$package_dir"
    done
}

function main() {
    pip_conf=$(which pip)
    if [[ $pip_conf == *"venv"* ]]; 
    then
        echo "Pip from venv in : $pip_conf"
        install_package
    else
        echo "Please use a venv (python -m venv venv && source venv/bin/activate) and install package in it."
        exit 1
    fi
}

main

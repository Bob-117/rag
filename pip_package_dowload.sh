#!/bin/bash

requirements=("langchain" "fastapi" "pypdf" "sentence-transformers" "chromadb==0.3.29")

download_dir="python_packages_for_llm_rag"
mkdir -p "$download_dir"

for package in "${requirements[@]}"; do
    echo -e "\n************\nDownloading package $package \n************\n"  
    package_dir="$download_dir/$package"
    # mkdir -p "$package_dir" && pip download "$package" --dest "$package_dir"
done

import warnings
from langchain._api import LangChainDeprecationWarning

warnings.simplefilter("ignore", category=LangChainDeprecationWarning)
# warnings.simplefilter(action="once", category=LangChainDeprecationWarning)

from langchain.chains.conversational_retrieval.base import ConversationalRetrievalChain
from langchain.chains.retrieval_qa.base import RetrievalQA

from langchain_community.llms.ollama import Ollama
from langchain_community.document_loaders import PyPDFLoader
from langchain.text_splitter import CharacterTextSplitter

from langchain_community.embeddings import OllamaEmbeddings, HuggingFaceEmbeddings
from langchain_community.vectorstores import Chroma
from fastapi import FastAPI
from pydantic import BaseModel
from langchain.memory import ChatMessageHistory, ConversationBufferMemory

MODEL = "tinyllama"
EMBEDDING_MODEL = "tinyllama" # use nomic-embed-text


def init_llm(_host='localhost', _port=11435):
    """
    Load a llm agent from local ollama 
    """
    print(f'Init local llm with model {MODEL}')
    _local_llm = Ollama(
        base_url=f'http://{_host}:{_port}',
        model=MODEL,
    )
    return _local_llm


local_llm = init_llm()


def load_one_pdf_for_rag():
    # Load  PDF
    loader = PyPDFLoader("/home/bob/Téléchargements/CV-28.pdf")
    data = loader.load()
    print(f'data : {data}')

    # Split text into chunks
    text_splitter = CharacterTextSplitter(chunk_size=1000, chunk_overlap=0)
    docs = text_splitter.split_documents(data)
    print(f'docs : {docs}')
    # print(data == docs)

    # Create embeddings
    use_ollama_embedding = True
    if use_ollama_embedding:
        embeddings = OllamaEmbeddings(
            base_url="http://localhost:11435",
            model=EMBEDDING_MODEL,
        )
    else:
        embeddings = HuggingFaceEmbeddings()

    # Store embeddings as vector in a vector store
    vector_store = Chroma.from_documents(docs, embeddings)
    print(f'vector store : {vector_store}')
    return vector_store


def question_answer_retrieval(_vector_store):
    """
    Create retrieval-based QA chain
    """
    qa = RetrievalQA.from_chain_type(
        llm=local_llm,
        chain_type="stuff",
        retriever=_vector_store.as_retriever(),
    )

    return qa


def question_answer_retrieval_with_memory(_vector_store):
    """
    Create retrieval-based QA chain
    + memory (ie context)
    + return source doc
    """
    message_history = ChatMessageHistory()
    memory = ConversationBufferMemory(
        memory_key="chat_history",
        output_key="answer",
        chat_memory=message_history,
        return_messages=True
    )

    chain = ConversationalRetrievalChain.from_llm(
        llm=local_llm,
        chain_type="stuff",
        retriever=_vector_store.as_retriever(),
        memory=memory,
        return_source_documents=True,
    )

    return chain


app = FastAPI()
__memory = ConversationBufferMemory(memory_key="chat_history")
vector = load_one_pdf_for_rag()
__qa = question_answer_retrieval(vector)

@app.get("/")
async def gibson():
    return {'gibson': 'sg'}


class QuestionRequest(BaseModel):
    question: str


@app.post("/question")
async def question(request: QuestionRequest):
    return {"answer": local_llm.invoke(request.question)}


@app.post("/question_with_rag")
async def question_with_rag(request: QuestionRequest):
    qa_response = __qa.run(request.question)
    print(f'MEMORY : {__memory}')
    return {"answer": qa_response}


if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host='0.0.0.0', port=5017)

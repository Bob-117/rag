import requests


# curl -X POST -H "Content-Type: application/json" -d '{"question": "What ?"}' http://localhost:5017/question


def __ask(_url, _payload, _headers):
    response = requests.post(_url, json=_payload, headers=_headers)

    if response.status_code == 200:
        print(response.json())
    else:
        print(f"Nope :\n- {response.status_code}\n- {_url}\n- {_payload}\n- {_headers}")


QUESTION = 'Quel age a X ?'

REQUESTS_PARAM = [
    ("http://0.0.0.0:5017/question", {"question": QUESTION}, {"Content-Type": "application/json"}),
    ("http://0.0.0.0:5017/question_with_rag", {"question": QUESTION}, {"Content-Type": "application/json"}),
]

if __name__ == '__main__':
    try:
        print(requests.get('http://0.0.0.0:5017/').json()) 
    except requests.exceptions.ConnectionError as e:
        print(f'App is not even up u monkey')
    else:
        for param in REQUESTS_PARAM:
            __ask(_url=param[0], _payload=param[1], _headers=param[2])

   
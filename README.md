# Rag

## Run Ollama

```
docker run -d  -v ollama_volume:/root/.ollama -p 11435:11434 --name ollama_service ollama/ollama
```

## Doc

### Doc 1
- https://github.com/ollama/ollama/blob/main/docs/tutorials/langchainpy.md
- https://github.com/ghif/langchain-tutorial/blob/main/15-ollama-rag-pdf.py
- https://github.com/sudarshan-koirala/rag-chat-with-pdf/blob/main/app.py
- https://github.com/SonicWarrior1/pdfchat/blob/master/app.py
- https://hackernoon.com/simple-wonders-of-rag-using-ollama-langchain-and-chromadb
- https://www.youtube.com/watch?v=7VAs22LC7WE + https://github.com/ThomasJay/RAG/blob/main/app.py
- https://github.com/pixegami/rag-tutorial-v2/tree/main
- https://medium.com/@meirgotroot/using-ollama-embedding-services-45d4d2a56640
- https://www.mongodb.com/developer/products/atlas/choose-embedding-model-rag/

- https://www.mongodb.com/developer/_next/image/?url=https%3A%2F%2Fimages.contentstack.io%2Fv3%2Fassets%2Fblt39790b633ee0d5a7%2Fblt2f97b4a5ed1afa1a%2F65eb340799cd92ca89c0c0b5%2Ftop-10-mteb.png&w=1080&q=75
- https://api.python.langchain.com/en/latest/embeddings/langchain_community.embeddings.ollama.OllamaEmbeddings.html
- https://github.com/zylon-ai/private-gpt/blob/main/private_gpt/components/embedding/embedding_component.py

- https://shuaib.org/technical-guide/how-to-update-or-upgrade-sqlite3-version-in-python/

- https://youtu.be/jENqvjpkwmw?feature=shared
- https://github.com/NVIDIA/GenerativeAIExamples/blob/main/notebooks/03_llama_index_simple.ipynb

- base de RAG : https://huggingface.co/datasets/MongoDB/cosmopedia-wikihow-chunked

- https://www.youtube.com/watch?v=2TJxpyO3ei4

- https://medium.com/@sudarshan-koirala/chat-with-pdf-using-chainlit-langchain-ollama-mistral-16da1144638c

### Doc 2

- https://realpython.com/build-llm-rag-chatbot-with-langchain/
- https://thenewstack.io/how-to-build-a-rag-powered-llm-chat-app-with-chromadb-and-python/
- https://thenewstack.io/how-to-build-a-rag-powered-llm-chat-app-with-chromadb-and-python/
- https://python.langchain.com/docs/modules/data_connection/document_loaders/pdf/
- https://github.com/langchain-ai/langchain
- https://pypi.org/project/langchain/
- https://medium.com/@w.bakkar/how-to-transfer-your-ollama-downloaded-llm-blobs-files-between-linux-and-windows-8cd4d33e5fcb
- https://github.com/ollama/ollama/issues/159
- https://github.com/ollama/ollama/blob/main/docs/tutorials/langchainpy.md
- https://www.featureform.com/post/the-definitive-guide-to-embeddings
- https://github.com/ggerganov/llama.cpp/blob/master/convert.py
- https://wiki.mutable.ai/ollama/ollama#macapp-source-code
- https://python.langchain.com/docs/use_cases/question_answering/quickstart/
- https://mlexplained.blog/2024/02/24/build-rag-application-using-ollama/
- https://www.youtube.com/watch?v=LfYEdLeH8m0
- https://github.com/ollama/ollama/blob/41434a7cdcf33918ae2d37eb23d819ef7361e843/docs/faq.md#how-can-i-change-where-ollama-stores-models
- https://github.com/ollama/ollama/issues/335
- https://gist.github.com/supersonictw/f6cf5e599377132fe5e180b3d495c553
- https://www.youtube.com/watch?v=fnvZJU5Fj3Q

- https://stackoverflow.com/questions/229551/how-to-check-if-a-string-contains-a-substring-in-bash


## How old is bob/ Quel age a Bob

```
Bob is an artificial intelligence system that was created by the developers of the popular video game "Star Wars: Battlefront II" in 2015. The official release date for this AI character was November 30, 2015, and it has since become a beloved character among fans of the original Star Wars trilogy and the Star Wars universe as a whole.
As one of the most popular characters in the video game "Star Wars: Battlefront II," Bob is often referred to by fans by his official name "Bob-7." His age, however, remains unknown to the general public because there are no reliable sources of information available regarding his specific creation date or development timeline.
However, the developers of "Star Wars: Battlefront II" have confirmed that Bob was created in 2015 by a team of programmers who worked on other projects at the time, and he has since become an iconic character in popular culture. The game's creators have also stated that they hope to add more characters from the Star Wars universe to the game in future updates or expansions.
Overall, Bob remains a beloved character among fans of "Star Wars: Battlefront II" and other video games featuring the Star Wars universe.
```

```
Noel est l'age de Bob # ???
```


## Retrieval Augmented Generation (RAG) python lib

```
# download, export, install
pip download langchain
mkdir rag_python_lib
mv * rag_python_lib
mv *.whl rag_python_lib
ls
ls rag_python_lib
cd rag_python_lib
mkdir langchain
mv * langchain
python3 -m venv venv
source venv/bin/activate
pip install langchain --no-index --find-links ../rag_python_lib/langchain/
pip list
```

```
# lib
pip install langchain
pip install fastapi
pip install pypdf
pip install sentence-transformers
pip install chromadb
pip install -I chromadb==0.3.29 # sqlite3 issues
```

# Export model

## Run 

```
docker volume create ollama_clean
docker volume inspect ollama_clean # /var/lib/docker/volumes/ollama_clean/_data/
docker run -d -v ollama_clean:/root/.ollama -p 11436:11434 --name ollama_service_clean ollama/ollama
```


```
# avant
root@bob:/home/bob# tree /var/lib/docker/volumes/ollama_clean/_data/
/var/lib/docker/volumes/ollama_clean/_data/
├── id_ed25519
├── id_ed25519.pub
└── models
    └── blobs

2 directories, 2 files
```

## Mistral

```
docker exec -it ollama_service_clean bash
ollama pull mistral
```

```
# pendant
root@bob:/home/bob# tree /var/lib/docker/volumes/ollama_clean/_data/
/var/lib/docker/volumes/ollama_clean/_data/
├── id_ed25519
├── id_ed25519.pub
└── models
    └── blobs
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-0
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-1
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-10
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-11
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-12
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-13
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-14
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-15
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-16
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-17
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-18
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-19
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-2
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-20
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-21
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-22
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-23
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-24
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-25
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-26
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-27
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-28
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-29
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-3
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-30
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-31
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-32
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-33
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-34
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-35
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-36
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-37
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-38
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-39
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-4
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-40
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-41
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-5
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-6
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-7
        ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-8
        └── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730-partial-9

2 directories, 45 files
```


```
# apres
root@bob:/home/bob# tree /var/lib/docker/volumes/ollama_clean/_data/
/var/lib/docker/volumes/ollama_clean/_data/
├── id_ed25519
├── id_ed25519.pub
└── models
    ├── blobs
    │   ├── sha256-43070e2d4e532684de521b885f385d0841030efa2b1a20bafb76133a5e1379c1
    │   ├── sha256-e6836092461ffbb2b06d001fce20697f62bfd759c284ee82b581ef53c55de36e
    │   ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730
    │   ├── sha256-ed11eda7790d05b49395598a42b155812b17e263214292f7b87d15e14003d337
    │   └── sha256-f9b1e3196ecfce03ac97468fa0b6d85941fea050c6666c57f5856a8baec6507d
    └── manifests
        └── registry.ollama.ai
            └── library
                └── mistral
                    └── latest

6 directories, 8 files
```

## Nomic-embed-text (rag)

```
ollama pull nomic-embed-text
```

```
root@bob:/home/bob# tree /var/lib/docker/volumes/ollama_clean/_data/
/var/lib/docker/volumes/ollama_clean/_data/
├── id_ed25519
├── id_ed25519.pub
└── models
    ├── blobs
    │   ├── sha256-31df23ea7daa448f9ccdbbcecce6c14689c8552222b80defd3830707c0139d4f
    │   ├── sha256-43070e2d4e532684de521b885f385d0841030efa2b1a20bafb76133a5e1379c1
    │   ├── sha256-970aa74c0a90ef7482477cf803618e776e173c007bf957f635f1015bfcfef0e6
    │   ├── sha256-c71d239df91726fc519c6eb72d318ec65820627232b2f796219e87dcf35d0ab4
    │   ├── sha256-ce4a164fc04605703b485251fe9f1a181688ba0eb6badb80cc6335c0de17ca0d
    │   ├── sha256-e6836092461ffbb2b06d001fce20697f62bfd759c284ee82b581ef53c55de36e
    │   ├── sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730
    │   ├── sha256-ed11eda7790d05b49395598a42b155812b17e263214292f7b87d15e14003d337
    │   └── sha256-f9b1e3196ecfce03ac97468fa0b6d85941fea050c6666c57f5856a8baec6507d
    └── manifests
        └── registry.ollama.ai
            └── library
                ├── mistral
                │   └── latest
                └── nomic-embed-text
                    └── latest

7 directories, 13 files
```

# ?

```
# mistral
sha256-43070e2d4e532684de521b885f385d0841030efa2b1a20bafb76133a5e1379c1
sha256-e6836092461ffbb2b06d001fce20697f62bfd759c284ee82b581ef53c55de36e
sha256-e8a35b5937a5e6d5c35d1f2a15f161e07eefe5e5bb0a3cdd42998ee79b057730
sha256-ed11eda7790d05b49395598a42b155812b17e263214292f7b87d15e14003d337
sha256-f9b1e3196ecfce03ac97468fa0b6d85941fea050c6666c57f5856a8baec6507d

```

```
# nomic
sha256-31df23ea7daa448f9ccdbbcecce6c14689c8552222b80defd3830707c0139d4f
sha256-970aa74c0a90ef7482477cf803618e776e173c007bf957f635f1015bfcfef0e6
sha256-c71d239df91726fc519c6eb72d318ec65820627232b2f796219e87dcf35d0ab4
sha256-ce4a164fc04605703b485251fe9f1a181688ba0eb6badb80cc6335c0de17ca0d
```

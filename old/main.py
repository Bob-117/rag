import PyPDF2
from langchain_community.document_loaders import PyPDFLoader
from langchain_community.embeddings import OllamaEmbeddings
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.llms.ollama import Ollama
from langchain_community.vectorstores import Chroma
from langchain.chains import ConversationalRetrievalChain
from langchain_community.chat_models import ChatOllama
from langchain.memory import ChatMessageHistory, ConversationBufferMemory
import chainlit as cl

MODEL = "tinyllama"

llm_local = Ollama(
    base_url='http://localhost:11435',
    model=MODEL
)
print(llm_local.invoke("Quel est l'age d'X ?"))

loader = PyPDFLoader('/home/bob/Téléchargements/CV-28.pdf')
documents = loader.load()

# embeddings = HuggingFaceEmbeddings() # nope ?
embeddings = OllamaEmbeddings(model=MODEL)
docsearch = Chroma.from_documents(documents, embeddings)

chain = ConversationalRetrievalChain.from_llm(llm_local, retriever=docsearch.as_retrieval_chain())

query = "Quel est l'age d'X?"
result = chain({"question": query, "chat_history": []})

print(result['answer'])